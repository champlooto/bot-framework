var ContextModel = require('../models/context');

exports.getContext = function(userId, accountId, profileNetworkId, profileNetworkName, callback) {
    var query = {
        userId : userId,
        accountId : accountId,
        profileNetworkId : profileNetworkId,
        profileNetworkName : profileNetworkName
    };

    exports.getContextByQuery(query, function(error, contextDoc) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getContext'
            };
            return callback(errorObj);
        }
        return callback(null, contextDoc);
    });
};

exports.getContextByUserIdNetworkInfo = function(userId, profileNetworkId, profileNetworkName, callback) {
    var query = {
        userId : userId,
        profileNetworkId : profileNetworkId,
        profileNetworkName : profileNetworkName
    };

    exports.getContextByQuery(query, function(error, contextDoc) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getContextByUserIdNetworkInfo'
            };
            return callback(errorObj);
        }
        return callback(null, contextDoc);
    });
};

exports.getContextByQuery = function(query, callback) {
    ContextModel.findOne(query, function(error, contextDoc) {
        if( error ) {
            return callback(error);
        }
        return callback(null, contextDoc);
    });
};

exports.upsertContext = function(doc, callback) {
    var query = {
        userId : doc.userId,
        accountId : doc.accountId,
        profileNetworkId : doc.profileNetworkId,
        profileNetworkName : doc.profileNetworkName
    };
    var update = {
        context : doc.context
    };
    var options = {
        upsert : true
    };

    ContextModel
        .findOneAndUpdate(query, update, options, function(error, contextDoc) {
            if(error) {
                var errorObj = {
                    error : error,
                    functionName : 'upsertContext'
                };
                return callback(errorObj);
            }
            return callback(null, contextDoc);
        });
};

exports.updateContext = function(query, updateDoc, otherInfo, callback) {
    ContextModel.update(query, updateDoc, otherInfo, function(error, result) {
        if(error)  {
            var errorObj = {
                error : error,
                functionName : 'updateContext'
            };
            return callback(errorObj);
        }
        return callback(null, result);
    });
};
