var ProductModel = require('../models/product');
var _ = require('underscore');

exports.getProductById = function(_id, accountId, callback) {

    var query = {
        _id : _id,
        accountId : accountId
    };

    ProductModel
    .findOne(query, function(error, product) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getProductById'
            };
            return callback(errorObj);
        }
        //Analytics productClick
        updateProductClick(_id,accountId,callback);
        return callback(null, product);
    });
};

exports.getProduct = function(query, skip, limit, sort, callback) {
    var cursor = ProductModel
                    .find(query)
                    .skip(skip)
                    .limit(limit);

    if( sort ) {
        cursor.sort( sort );
    }

    cursor
    .exec(function(error, productList) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getProduct'
            };
            return callback(errorObj);
        }

        //Analytics productImpression
        var accountId = query.accountId;
        upadateProductImpression(accountId, productList, function(){});
        return callback(null, productList);
    });
};

exports.getProductByAggregateQuery = function(accountId, text, gender, colorList, params, skip, limit, callback) {

    var orColorCondition = _.map(colorList, function(color) {
        return { $eq : ['$color', color] };
    });

    var query = {
        accountId : accountId
    };

    if( text && text.trim().length > 0 ) {
        query['$text'] = {
            $search : text.trim().toLowerCase()
        };
    }

    query = _.extend(query, params);


    ProductModel.aggregate(
        [
            {
                $match : query
            },
            {
                $project : {
                    color : {
                        $cond : [
                            { $eq : [ "$color", [] ] },
                            ['null'],
                            {
                                $ifNull : ['$color', ['null']]
                            }
                        ]
                    },
                    document : '$$ROOT'
                }
            },
            {
                $unwind : '$color'
            },
            {
                $project : {
                    score : {
                        $meta : 'textScore'
                    },
                    genderMatch : {
                        $cond : [{ $eq : ['document.$gender', gender] }, 1, 0]
                    },
                    colorMatch : {
                        $cond : [ { $or : orColorCondition}, 1, 0]
                    },
                    document : '$$ROOT'
                }
            },
            {
                $group : {
                    _id : '$_id',
                    textScoreCount : {$avg : '$score'},
                    genderMatchCount : {$sum : '$genderMatch'},
                    colorMatchCount : {$sum : '$colorMatch'},
                    original_doc : {
                        $first : '$document'
                    }
                }
            },
            {
                $sort : {
                    genderMatchCount : -1,
                    colorMatchCount : -1,
                    textScoreCount : -1,
                    'original_doc.document.price' : 1,
                    'original_doc.document.createdAt' : -1
                }
            },
            {
                $skip : skip
            },
            {
                $limit : limit
            }
        ],
        function(error, result) {
            if( error ) {
                var errorObj = {
                    error : error,
                    functionName : 'getProductByTextSearch'
                };
                return callback(errorObj);
            }

            var productList = _.map(result, function(doc) {
                var originalDocument = doc.original_doc.document;
                //console.log('score', doc.textScoreCount, doc.colorMatchCount, doc.original_doc.price, doc.original_doc.createdAt);
                //console.log('text', originalDocument.productName);
                //console.log('tagList', originalDocument.tagList);
                //console.log(doc);
                return originalDocument;
            });

            //Analytics productImpression
            upadateProductImpression(accountId, productList, function(){});
            return callback(null, productList);
        }
    );
};

exports.getLatestProduct = function(accountId, skip, limit, callback) {
    var query = {
        accountId : accountId
    };
    var sort = {
        createdAt : -1
    };

    exports.getProduct(query, skip, limit, sort, function(error, productList) {
        return callback(error, productList);
    });
};

//Analytics productClick
var updateProductClick = function(_id, accountId, callback){
    var query = {
        accountId : accountId,
        _id : _id
    };
    var condition = {$inc: {productClick:1}};
    incrementmetrics(query, condition, 'updateProductClick', callback);
}

//Analytics productImpression
var upadateProductImpression = function(accountId, productList, callback){
    productList.forEach(function(product){
        var query={
            accountId : accountId,
            _id : product._id
        };
        var condition = {$inc: {productImpression:1}};
        incrementmetrics(query, condition,'upadateProductImpression', callback);
    });
};

// Analytics Metrics Incrementer
var incrementmetrics = function(query, condition, functionName, callback){
    ProductModel.update(query, condition, {upsert:true}).exec(function(error){
        if(error) {
            var errorObj = {
                error : error,
                functionName : functionName
            };
            return callback(errorObj);
        }
        return;
    });
};



//var accountId = '578e72a8e020be8c709d189d';
//var accountId = '57f815da0574a4c00c79013d';
//var gender = null;
//var color = [];
//var text = '';
//var price = 100;
//var params = {
//    //storeId : '578e76dfe020be8c709d189f'
//    storeId : '57f815e10574a4c00c79013e'
//};
//
//exports.getProductByAggregateQuery(accountId, text.toLocaleLowerCase(), gender, color, params, 0, 5, function(error, productList) {
//     console.log(error, productList);
//});