var ComplainModel = require('../models/complain');

exports.getComplainListByAccountId = function(accountId, callback) {
    var query = {
        accountId : accountId
    };
    getComplainList(query, function(error, complainList) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getComplainListByAccountId'
            };
            return callback(errorObj);
        }
        return callback(null, complainList);
    });
};

var getComplainList = function(query, callback) {
    ComplainModel
    .find(query)
    .exec(function(error, complainList) {
        if(error) {
            return callback(error);
        }
        return callback(null, complainList);
    });
};

exports.createComplain = function(obj, callback) {
    var complainModel = new ComplainModel(obj);
    complainModel.save(function(error, newComplain) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'createComplain'
            };
            return callback(errorObj);
        }
        return callback(null, newComplain);
    });
};

exports.editComplain = function(_id, isResolved, accountId, callback) {
    var query = {
        _id : _id,
        accountId : accountId
    };
    var updateObj = {
        isResolved : isResolved
    };
    ComplainModel
    .find(query)
    .update(updateObj, function(error, result) {
        if(error) {
            var errorObj = {
                functionName : 'editComplain',
                error : error
            };
            return callback(errorObj);
        }
        return callback(null, result);
    });
};
