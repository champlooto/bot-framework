var CustomerModel = require('../models/customer.js');


exports.getData = function(accountId,callback){
  var query = {
      accountId : accountId
  };
  CustomerModel
  .findOne(query)
  .exec(function(error, customerData) {
      if(error) {
          return callback(error);
      }
      return callback(null, customerData);
  });
};


exports.updateData = function(accountId,userCount, interactionCount,callback){
  var query={
    accountId: accountId,
  }
  var update = {
    accountId : accountId,
    customerCount : userCount,
    interactionCount : interactionCount
  }
  CustomerModel.findOneAndUpdate(query, {$set:update}, {upsert:true}, function(error, customerData){
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'updateData'
            };
            return callback(errorObj);
        }
        return callback(null, customerData);
    });
}
