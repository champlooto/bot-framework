var UserModel = require('../models/user');

exports.createUser = function(obj,callback){
  var userModel = new userModel(obj);
  userModel.save(function(error, newUser) {
      if(error) {
          var errorObj = {
              error : error,
              functionName : 'createUser'
          };
          return callback(errorObj);
      }
      return callback(null, newUser);
  });
};
}

exports.updateUser = function(accountId,userId, likedProduct, callback){
  var query = {
    accountId : accountId,
    userId : userId,
  };
  var updateObj = {
    likedProduct : likedProduct,
  };
  UserModel
  .find(query)
  .upsert(updateObj, function(error, result) {
      if(error) {
          var errorObj = {
              functionName : 'updateUser',
              error : error
          };
          return callback(errorObj);
      }
      return callback(null, result);
  });
}

var getUser = function(accountId,userId, callback) {
    var query = {
      accountId : accountId,
      userId : userId
  };
    UserModel
    .find(query)
    .exec(function(error, user) {
        if(error) {
            return callback(error);
        }
        return callback(null, user);
    });
};
