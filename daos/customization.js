var CustomizationModel = require('../models/customization');

exports.getCustomizationByAccountId = function(accountId, callback) {
    var query = {
        accountId : accountId,
    };
    CustomizationModel
    .findOne(query)
    .exec(function(error,customization) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getCustomizationByAccountId'
            };
            return callback(errorObj);
        }
        return callback(null, customization);
    });
};
