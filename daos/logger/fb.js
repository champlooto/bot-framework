var FbLoggerModel = require('../../models/logger/fb');

exports.create = function(doc, callback) {
    var fbLoggerModel = new FbLoggerModel(doc);
    fbLoggerModel.save(function(error, result) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'create'
            };
            return callback(errorObj);
        }
        return callback(null, result);
    });
};