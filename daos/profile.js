var ProfileModel = require('../models/profile');

exports.getProfileByNetworkIdAndName = function(profileNetworkId, profileNetworkName, callback) {
    var query = {
        profileNetworkId : profileNetworkId,
        profileNetworkName : profileNetworkName
    };
    ProfileModel
    .findOne(query, function(error, profileDoc) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getProfileByNetworkIdAndName'
            };
            return callback(errorObj);
        }
        return callback(null, profileDoc);
    });
};
