var StoreModel = require('../models/store');
var _ = require('underscore');

exports.getStoreList = function(accountId, callback) {
    var query = {
        accountId : accountId,
        isActive : {
            $in : [true, null]
        }
    };
    getStoreList(query, null, null, function(error, storeList) {
        if( error ) {
            var errorObj = {
                error : error,
                functionName : 'getStoreList'
            };
            return callback(errorObj);
        }
        return callback(null, storeList);
    });
};

exports.getStoreByName = function(accountId, name, params, callback) {
    var query = {
        accountId : accountId,
        name : {
            $regex : name,
            $options : 'i'
        }
    };
    query = _.extend(query, params);

    getStoreList(query, null, null, function(error, storeList) {
        if( error ) {
            var errorObj = {
                error : error,
                functionName : 'getStoreByName'
            };
            return callback(errorObj);
        }
        return callback(null, storeList);
    });
};

var getStoreList = function(query, skip, limit, callback) {

    var cursor = StoreModel.find(query);

    if( skip ) {
        cursor = cursor.skip(skip);
    }
    if( limit ) {
        cursor = cursor.limit(limit);
    }

    cursor.exec(function(error, storeList) {
        if(error) {
            return callback(error);
        }
        return callback(null, storeList);
    });
};