var _ = require('underscore');
var CategoryModel = require('../models/category');
var mongoose = require('mongoose');

exports.getCategoryListByStoreId = function(storeId, accountId, callback) {
    var query = {
        storeId : storeId,
        accountId : accountId,
        isActive : {
            $in : [true, null]
        }
    };
    getCategoryList(query, null, null, function(error, categoryList) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getCategoryListByStoreId'
            };
            return callback(errorObj);
        }
        return callback(null, categoryList);
    });
};

exports.getCategoryListByAccountId = function(accountId, callback) {
    var query = {
        accountId : accountId,
        isActive : {
            $in : [true, null]
        }
    };
    getCategoryList(query, null, null, function(error, categoryList) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getCategoryListByAccountId'
            };
            return callback(errorObj);
        }
        return callback(null, categoryList);
    });
};

exports.getCategoryByName = function(accountId, name, params, callback) {
    var query = {
        accountId : accountId,
        name : {
            $regex : name,
            $options : 'i'
        }
    };

    query = _.extend(query, params);

    getCategoryList(query, null, null, function(error, categoryList) {
        if(error) {
            var errorObj = {
                error : error,
                functionName : 'getCategoryByName'
            };
            return callback(errorObj);
        }
        return callback(null, categoryList);
    });
};

exports.getCategoryListByIdList = function(idList, accountId, params, callback) {

    var qIdList = _.map(idList, function(id) {
        return mongoose.Types.ObjectId(id);
    });

    var query = {
        _id : {
            $in : qIdList
        },
        accountId : accountId
    };
    query = _.extend(query, params);

    getCategoryList(query, null, null, function(error, categoryList) {
        if( error ) {
            var errorObj = {
                error : error,
                functionName : 'getCategoryListByIdList'
            };
            return callback(errorObj);
        }
        return callback(null, categoryList);
    });
};


var getCategoryList = function(query, skip, limit, callback) {
    var cursor = CategoryModel.find(query);

    if( skip ) {
        cursor = cursor.skip(skip);
    }
    if( limit ) {
        cursor = cursor.limit(limit);
    }

    cursor.exec(function(error, categoryList) {
        if(error) {
            return callback(error);
        }
        return callback(null, categoryList);
    });
};
