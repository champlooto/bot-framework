var express = require('express');
var router = express.Router();
var FacebookHandlerService = require('../services/facebook/handler');

var VERIFICATION_MESSAGE = 'champloo facebook webhook verification message';

router.get('/webhook', function(req, res, next) {
    if (req.query['hub.verify_token'] === VERIFICATION_MESSAGE) {
        return res.send(req.query['hub.challenge']);
    }
    return res.send('Error, wrong validation token');
});

router.post('/webhook', function(req, res, next) {
    var body = req.body;
    res.sendStatus(200);
    FacebookHandlerService.processBody(body);
});

module.exports = router;
