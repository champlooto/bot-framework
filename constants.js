exports.V1_API = '/v1';

exports.ERROR_CODE = 0;
exports.SUCCESS_CODE = 1;
exports.RESTART = 'restart';
exports.HUMAN = 'help';
exports.BOT = 'shop';

exports.API = {
    FACEBOOK : exports.V1_API + '/facebook',
};

exports.FACEBOOK_API = {
    SEND_MESSAGE : 'https://graph.facebook.com/v2.6/me/messages',
    BASE_URL : 'https://graph.facebook.com/v2.6'
};

exports.MESSAGE_TYPE = {
    TEXT : 'text',
    PRODUCT : 'product',
    OPTION : 'option',
    QUICK_REPLY : 'quickReply'
};

exports.CONTEXT_VERSION = 'v2';
exports.PRODUCT_COUNT_LIMIT = 5;
