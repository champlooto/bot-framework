var ActionsService = require('./actions');
var WitService = require('./wit');
var config = require('../config');
var WIT_ACCESS_TOKEN = config.WIT_ACCESS_TOKEN;
var witService = new WitService(WIT_ACCESS_TOKEN);
var BufferResponses = require('./buffer-responses');
var ContextParser = require('./context-parser');
var CustomerDao = require('../daos/customer');
var constants = require('../constants');
var RESTART = constants.RESTART;
var HUMAN = constants.HUMAN;
var BOT = constants.BOT;
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var _ = require('underscore');
var natural = require('natural');

exports.textMessageHandler = function(userId, accountId, text, context, callback) {

    // Analytics Interactions Tracking
    analyticsCustomerData(accountId, context);

    // Supporting Text Response to a quickReply action
    var messageQueue = context.getVal('previousMessageQueue');
    if( messageQueue && messageQueue.length > 0 ) {
        var lastMessage = messageQueue[messageQueue.length-1];
        if (lastMessage.type === MESSAGE_TYPE.QUICK_REPLY) {
            var matchingPayload;
            tokenizer = new natural.WordTokenizer();
            var options = _.map(lastMessage.data, function (option) {
                return tokenizer.tokenize(option.title).join(' ')
            });
            var CURRENT_MAX_VAL = 0.9;
            for (var option in options) {
                if (natural.JaroWinklerDistance(options[option], text) >= CURRENT_MAX_VAL) {
                    matchingPayload = lastMessage.data[option].payload;
                    CURRENT_MAX_VAL = natural.JaroWinklerDistance(options[option], text);
                }
            }
            if (matchingPayload) {
                var payload = matchingPayload;
                return exports.postbackHandler(userId, accountId, payload, context, callback);
            }
        }
    }

    //talk to the bot command check
    if ( natural.JaroWinklerDistance(text.toLowerCase(), BOT) > 0.95 ) {
        var payload = 'bot,true,purchaseOption';
        return exports.postbackHandler(userId, accountId, payload, context, callback);
    }

    //Supporting NO reply and restart for leaveMessageActions
    var previousActionList = context.getVal('previousActionList');
    if( previousActionList && previousActionList.length > 0 ) {
        var previousAction = previousActionList[previousActionList.length - 1];
        if (previousAction === 'leaveMessageAction') {
            if (natural.JaroWinklerDistance(text.toLowerCase(), RESTART) > 0.90) {
                var payload = 'intent,shop,purchaseOption';
                return exports.postbackHandler(userId, accountId, payload, context, callback);
            }
            else {
                var payload = 'intent,complaint,leaveMessage';
                return exports.postbackHandler(userId, accountId, payload, context, callback);
            }
        }
    }

    //talk to a human command check
    if ( natural.JaroWinklerDistance(text.toLowerCase(), HUMAN) > 0.95 ) {
        var payload = 'intent,complaint,leaveMessage';
        return exports.postbackHandler(userId, accountId, payload, context, callback);
    }

    // if text is not handled it is left for wit to be handled
    witService.parseMessage(userId, text, context, function(error, contextChangeList, actionList) {

        contextChangeList.push({
            contextKey : 'text',
            contextValue : text
        });

        contextChangeList.push({
            contextKey : 'wit',
            contextValue : true
        });

        //Analytics Inclusion
        if( context.getVal('intent') === 'complaint' ) {
            contextChangeList.push({
                contextKey : 'complaint',
                contextValue : text,
            });
            contextChangeList.push({
                contextKey : 'userId',
                contextValue : userId
            });
        }
        parseContextAndExecuteActionList(accountId, actionList, context, contextChangeList, callback);
    });
};


//payload is a comma separated string
exports.postbackHandler = function(userId, accountId, payload, context, callback) {

    analyticsCustomerData(accountId, context);

    var splitArr = payload.split(',');
    var contextKey = splitArr[0];
    var contextValue;

    if( contextKey === 'intent' ) {
        contextValue = splitArr[1];
    }
    else {
        contextValue = [{
            value : splitArr[1],
            wit : false
        }];
    }

    //set bot true
    if( contextKey === 'bot' ) {
        contextValue = true;
    }

    var actionList = [];

    //if action is present
    if( splitArr.length > 2 ) {
        var action = splitArr[2] + 'Action';
        actionList.push({wit: false, action : action, confidence : 1});
    }

    var contextChangeList = [];

    contextChangeList.push({
        contextKey : contextKey,
        contextValue : contextValue
    });

    contextChangeList.push({
        contextKey : 'wit',
        contextValue : undefined,
        delete : true
    });

    contextChangeList.push({
        contextKey : 'text',
        contextValue : undefined,
        delete : true
    });

    parseContextAndExecuteActionList(accountId, actionList, context, contextChangeList, callback);
};


var parseContextAndExecuteActionList = function(accountId, oldActionList, oldContext, contextChangeList, callback) {

    var resultObj = ContextParser.getContextAndAction(oldContext, contextChangeList, oldActionList);
    var context = resultObj.context;
    var actionList = resultObj.actionList;

    if( actionList.length > 0 ) {
        var firstAction = actionList[0];
        var bufferResponseData = BufferResponses[firstAction];
        if( bufferResponseData ) {
            callback(null, context, bufferResponseData.messageQueue);
        }
    }

    ActionsService.executeActionList(accountId, actionList, context, function(error, newContext, messageQueue) {
        if(error) {
            return callback(error);
        }

        return callback(null, newContext, messageQueue);
    });
};

// Analytics Interactions Tracking
var analyticsCustomerData = function(accountId, context) {
    CustomerDao.getData(accountId, function(error, customerData) {
        if( error ) {
            return console.log(error);
        }

        if( customerData ) {
            var interactionCount = customerData.interactionCount + 1;

            //unique user count is not accurate
            if( _.isEmpty( context.getVal('actionEntities') ) ) {
                var userCount = customerData.customerCount + 1;
            }
            else {
                var userCount = customerData.customerCount;
            }
        }
        else {
            var interactionCount = 1;
            var userCount = 1;
        }
        CustomerDao.updateData(accountId, userCount, interactionCount, function(){});
    });
 };
