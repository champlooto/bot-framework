var Wit = require('node-wit').Wit;
var async = require('async');
var _ = require('underscore');
var Context = require('./context');
var constants = require('../constants');

function WitService(ACCESS_TOKEN) {
    var actions = {
        say : function(sessionId, context, message, cb) {
            cb();
        },
        merge : function(sessionId, context, entities, message, cb) {
            cb(context);
        },
        error : function(sessionId, context, error) {
            console.log('e', error.message);
        }
    };
    this.client = new Wit(ACCESS_TOKEN, actions);
};

//keeping context for legacy reasons may need it in the future
WitService.prototype.multiConverse = function(id, message, context, callback) {
    var _this = this;
    var client = this.client;
    var execute = true;
    var actionList = [];
    var contextChangeList = [];

    //need to set new session on wit on every request
    var uniqueSessionId = id + '_' + new Date().getTime();
    var clonedContext = new Context();

    async.whilst(
        function() {
            return execute;
        },
        function(loopCallback) {
            var witActionEntities = clonedContext;

            //console.log(witActionEntities);
            client.converse(uniqueSessionId, message, witActionEntities, function(error, converseResult) {
                message = null;
                var type = converseResult.type;
                switch (type) {
                    case 'stop':
                        //end while loop
                        execute = false;
                        break;
                    case 'merge':
                        //console.log('merge');
                        //console.log(converseResult);
                        var entities = converseResult.entities;
                        _this.merge(clonedContext, entities);
                        contextChangeList = _this.addToContextChangeList(contextChangeList, entities);
                        break;
                    case 'action':
                        //console.log('action');
                        //console.log(converseResult);
                        var entities = converseResult.entities;
                        _this.merge(clonedContext, entities);
                        contextChangeList = _this.addToContextChangeList(contextChangeList, entities);
                        actionList.push(converseResult);
                        break;
                    default:
                        //end while loop by default
                        execute = false;
                }
                loopCallback();
            });
        },
        function(error, result) {
            //console.log(JSON.stringify(contextChangeList), actionList);
            return callback(error, contextChangeList, actionList);
        }
    );
};

WitService.prototype.parseMessage = function(id, message, context, callback) {
    this.multiConverse(id, message, context, function(error, clonedContext, actionList) {
        return callback(error, clonedContext, actionList);
    });
};

WitService.prototype.merge = function(clonedContext, entities) {
    if( entities ) {
        var keys = Object.keys(entities);
        keys.forEach(function (key) {
            var keyToUse = key;
            if( key === 'amount_of_money' ) {
                keyToUse = 'price';
            }
            clonedContext.setVal(keyToUse, entities[key]);
        });
    }
};

WitService.prototype.addToContextChangeList = function(contextChangeList, entities) {

   if( entities ) {

       _.each(entities, function (entityValueList, entityName, obj) {

           _.each(entityValueList, function (entityObj) {
               entityObj.wit = true;
           });

           if( entityName === 'amount_of_money' ) {
               entityName = 'price';
           }

           contextChangeList.push({
               contextKey: entityName,
               contextValue: entityValueList
           });
       });
   }

    return contextChangeList;
};

module.exports = WitService;
