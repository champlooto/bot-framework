var _ = require('underscore');
var constants = require('../constants');
var Context = require('./context');

//decision making based on actions confidence and entities
exports.getContextAndAction = function(context, contextChangeList, rawActionList) {

    //if user doesn't want to shop then just keep quiet
    var witObj = _.findWhere(contextChangeList, {contextKey : 'wit', contextValue : true});

    //redirect to greeting message for first time user
    if( context.getVal('firstTime') && context.getVal('firstTime') !== 'no' ) {
        rawActionList = [{ action : 'greetingAction' }];
    }

    //check for other language
    //var isOtherLanguage = checkIfOtherLanguage(contextChangeList);

    //if( isOtherLanguage ) {
    //
    //    //no action for other language
    //    rawActionList = [];
    //}

    var actionList = [];

    //action needs a confidence of atleast 0.02 to work
    var rawAction = rawActionList.length > 0 ? rawActionList[0] : null;
    if( rawAction ) {
        actionList = [rawAction.action];
    }

    var action;
    if( actionList.length > 0 ) {
        action = actionList[0];
    }

    //change context according to the action
    contextChangeList = exports.getContextChanges(context, contextChangeList, action);


    if( action === 'latestProductAction' ) {
        actionList = ['findProductAction'];
    }

    //handle in the next release
    if( action === 'oridinalAction' || action === 'numberAction' || action === 'helpAction' ) {
        actionList = [];
    }

    //console.log(JSON.stringify(contextChangeList));

    //apply context changes
    context = exports.applyContextChanges(context, contextChangeList);

    //for custom users
    var noAI = context.getVal('noAI');
    if( noAI ) {
        if( witObj ) {
            if( actionList[0] != 'greetingAction' ) {
                actionList = ['emptyAction'];
            }
        }
        else if( actionList[0] === 'purchaseOptionAction'|| actionList[0] === 'storeAction' ) {
            actionList = ['customStoreAction'];
        }
        else if( actionList[0] === 'findProductAction' ) {
            actionList = ['customFindAction'];
        }
    }

    var result = {
        context : context,
        actionList : actionList
    };

    return result;
};

var checkIfOtherLanguage = function(contextChangeList) {

    try {

        for (var index in contextChangeList) {
            var changeObj = contextChangeList[index];

            if (changeObj.contextKey === 'keyword' || changeObj.contextKey === 'greeting') {

                //replace all non-alpha numeric characters and check if value
                var value = changeObj.contextValue[0].value.replace(/\W+/g, '');
                if ( value.length === 0 ) {
                    return true;
                }
            }
        }
    }
    catch(e) {
        //ignore exception
    }

    return false;
};

//returns a delete doc for the given key
var getResetObj = function(key) {
    return {
        contextKey : key,
        delete : true
    };
};


exports.getContextChanges = function(oldContext, contextChangeList, action) {

    var concatKeys = ['likedProductId'];
    _.each(concatKeys, function( key ) {
        var obj = _.findWhere(contextChangeList, {contextKey : key});
        if( obj ) {
            var currentList = oldContext.getVal(key) || [];
            var newVal = obj.contextValue;
            var alreadyExists = _.findWhere(currentList, {value : newVal});
            if( !alreadyExists ) {
                currentList.push(obj.contextValue[0]);
            }
            obj.contextValue = currentList;
        }
    });

    //if these actions are called then reset the context object
    if( action === 'greetingAction'
        || action === 'purchaseOptionAction'
        || action === 'storeEnquiryAction'
        || action === 'latestProductAction'
    ) {
        var productKeys = ['productPageCount', 'color', 'price', 'keyword', 'gender', 'store', 'category', 'likedProductId'];
        _.each(productKeys, function(key) {
            contextChangeList.push(getResetObj(key));
        });
    }

    return contextChangeList;
};

exports.applyContextChanges = function(context, contextChangeList) {

    _.each(contextChangeList, function(contextChangeObj) {
        var contextKey = contextChangeObj.contextKey;
        var contextValue = contextChangeObj.contextValue;

        if( contextChangeObj.delete ) {
            context.deleteVal(contextKey);
        }
        else {
            context.setVal(contextKey, contextValue);
        }
    });

    return context;
};
