var async = require('async');
var FacebookTemplateService = require('./template');
var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var FacebookApiService = require('./api');

exports.send = function(messageQueue, userId, accessToken, callback) {
    async.eachSeries(messageQueue, function(message, next) {

        var type = message.type;
        var data = message.data;
        var template;
        switch (type) {
            case MESSAGE_TYPE.TEXT:
                template = FacebookTemplateService.getTextMessageTemplate(data);
                break;
            case MESSAGE_TYPE.PRODUCT:
                template = FacebookTemplateService.getProductsTemplate(data);
                break;
            case MESSAGE_TYPE.OPTION:
                var title;
                if(message.title) {
                    title = message.title;
                }
                template = FacebookTemplateService.getOptionsTemplate(data, title);
                break;
            case MESSAGE_TYPE.QUICK_REPLY:
                var title;
                if(message.title){
                  title = message.title;
                }
                template = FacebookTemplateService.getQuickReplyTemplate(data, title);
                break;
            default:
                return next();
        }

        if(template) {
            FacebookApiService.sendMessage(userId, template, accessToken, function(error, result) {
                if(error) {
                    console.log(error);
                }
                return next();
            });
        }
        else {
            return next();
        }

    }, function(error) {
        return callback();
    });
};
