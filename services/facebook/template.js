var _ = require('underscore');

exports.getTextMessageTemplate = function(text) {
  var text = text.slice(0,319);
    return {
        text : text
    };
};

exports.getProductsTemplate = function(products) {
    var elements = _.map(products, function(product, index) {
        var element = {
            title : product.name ? product.name.slice(0,79) : 'Product',
            buttons : getOptionButtons(product.options)
        };

        if( product.imageUrl ) {
            element.image_url = product.imageUrl;
        }

        //add subtitle
        if( product.subtitle ) {
            element.subtitle = product.subtitle;
        }
        return element;
    });

    var template = {
        attachment : {
            type : 'template',
            payload : {
                template_type: 'generic',
                elements : elements
            }
        }
    };
    return template;
};

exports.getOptionsTemplate = function(options, title) {
    title = title ? title.slice(0,319) : 'Options';
    var buttons = getOptionButtons(options);
    var template = {
        attachment : {
            type : 'template',
            payload: {
                template_type : "generic",
                elements : [
                    {
                        title : title,
                        buttons : buttons
                    }
                ]
            }
        }
    };
    return template;
};

exports.getQuickReplyTemplate = function(options, title) {
    title = title ? title.slice(0, 319) : '.';
    var options = options.slice(0, 9);
    var quickReplies = _.map(options, function(option) {
        var obj = {
            content_type : 'text',
            title : option.title.slice(0, 19),
            payload : option.payload
        };
        return obj;
    });
    var template = {
        text: title,
        quick_replies : quickReplies
    };
    return template;
};

var getOptionButtons = function(options) {
    //only first three buttons
    var options = options.slice(0,3);
    var buttons = _.map(options, function(option) {
        if( option.payload ){
            var obj = {
                type : 'postback',
                title : option.title.slice(0,19),
                payload : option.payload
            };
        }
        else if( option.title === 'Share' ) {
            var obj = {
                type : 'element_share'
            };
        }
        else {
            var obj = {
                type : 'web_url',
                title : option.title.slice(0,19),
                url : option.url,
            }
        }

        return obj;
    });
    return buttons;
};
