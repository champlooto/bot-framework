var constants = require('../../constants');
var SEND_MESSAGE_API = constants.FACEBOOK_API.SEND_MESSAGE;
var BASE_URL_API = constants.FACEBOOK_API.BASE_URL;
var request = require('request');

exports.sendMessage = function(userId, messageData, access_token, callback) {
    // console.log(messageData);
    request(
        {
            url: SEND_MESSAGE_API,
            qs: { access_token: access_token },
            method: 'POST',
            json: {
                recipient: { id : userId },
                message: messageData
            }
        }, function(error, response, body) {
            return commonResponseHandler(error, response, body, 'sendMessage', callback);
    });
};


exports.getUserInfo = function(userId, access_token, callback) {

    request(
        {
            url : BASE_URL_API + '/' + userId,
            qs : {access_token : access_token},
            method : 'GET'
        }, function(error, response, body) {
            return commonResponseHandler(error, response, body, 'getUserInfo', callback);
        }
    )
};


var commonResponseHandler = function(error, response, body, functionName, callback) {
    try {
        if(typeof response.body === 'string') {
            response.body = JSON.parse(response.body);
        }
    }
    catch(e) {
        console.log(e);
    }

    if (error) {
        var errorObj = {
            error : error,
            functionName : functionName
        };
        return callback(errorObj);
    }

    else if (response.body && response.body.error) {
        var errorObj = {
            error : response.body.error,
            functionName : functionName
        }
        return callback(errorObj);
    }

    else if(response.body && !response.body.error) {
        return callback(null, response.body);
    }

    else {
        var errorObj = {
            error : 'No response body',
            functionName : functionName
        };
        return callback(errorObj);
    }
};
