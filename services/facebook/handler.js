var async = require('async');
var CommonHandler = require('../common-handler');
var Context = require('../context');
var ContextDao = require('../../daos/context');
var FacebookResponseService = require('./response');
var FacebookApiService = require('./api');
var ProfileDao = require('../../daos/profile');
var CustomizationDao = require('../../daos/customization');
var Logger = require('./logger');

exports.processBody = function(body) {
    var entries = body.entry;

    async.each(entries, function(entry, messageCallback) {
        var messaging_events = entry.messaging;
        async.each(messaging_events, function(event, eventCallback) {

            //handle message echoes
            if( event.message && event.message.is_echo ) {
                echoHandler(event);
                return eventCallback();
            }

            var userId = event.sender.id;
            var profileNetworkId = event.recipient.id;
            var profileNetworkName = 'facebook';

            //first get the profileDoc from db
            ProfileDao.getProfileByNetworkIdAndName(profileNetworkId, profileNetworkName, function(error, profileDoc) {
                if( error || !profileDoc ) {
                    return eventCallback();
                }

                //if user has disabled the bot then stop answering
                if( profileDoc.isActive === 0 ) {
                    return eventCallback();
                }

                var accessToken = profileDoc.token;
                var accountId = profileDoc.accountId;
                var storeName = profileDoc.profileUsername;

                CustomizationDao.getCustomizationByAccountId(accountId, function(error, customizationDoc) {
                    if(error){
                        console.log(error);
                    }
                    var noAI = (customizationDoc && customizationDoc.noAI) ? customizationDoc.noAI : 0;

                    ContextDao.getContext(userId, accountId, profileNetworkId, profileNetworkName, function(error, contextDoc) {
                        if( error ) {
                            console.log(error);
                            return eventCallback();
                        }

                        //Don't need this now
                        //check if the page admin was talking to the user or not
                        //if( contextDoc && contextDoc.restartTimestamp ) {
                        //    var currentTimestamp = new Date().getTime();
                        //    var timeDiff = contextDoc.restartTimestamp - currentTimestamp;
                        //
                        //    //if the restartTimestamp is greater than currentTimestamp than don't talk to the user
                        //    if( timeDiff > 0 ) {
                        //        return eventCallback();
                        //    }
                        //}

                        //get the context object
                        var rawContext = contextDoc && contextDoc.context ? contextDoc.context : {};
                        //var rawContext = {};
                        var context = new Context(rawContext);

                        //by default show firstTime message to all users
                        if( !context.getVal('firstTime') ) {
                            context.setVal('firstTime', true);
                        }
                        else {
                            context.setVal('firstTime', 'no');
                        }

                        //need to store the messenger platform and pageName
                        context.setVal('network', 'facebook');
                        context.setVal('storeName', storeName);
                        context.setVal('userId', userId);
                        context.setVal('noAI', noAI);

                        //callback after reading the context
                        var callbackAfterParsingContext = function(error, newContext, messageQueue) {
                            var doc = {
                                userId : userId,
                                accountId : accountId,
                                profileNetworkId : profileNetworkId,
                                profileNetworkName : profileNetworkName,
                                context : newContext
                            };

                            ContextDao.upsertContext(doc, function(error, result) {});

                            //send response to facebook
                            FacebookResponseService.send(messageQueue, userId, accessToken, function() {});
                        };

                        var executeAfterFbUserInfoApiCall = function() {

                            //text message handler
                            if ( event.message && event.message.text && !event.message.quick_reply ) {
                                var text = event.message.text;
                                console.log(text);
                                textMessageHandler(userId, accountId, text, context, callbackAfterParsingContext);
                            }

                            //postback handler
                            else if ( event.postback && event.postback.payload ) {
                                postbackHandler(userId, accountId, event.postback.payload, context, callbackAfterParsingContext);
                            }

                            //quickReply handler
                            else if ( event.message && event.message.quick_reply && event.message.quick_reply.payload ) {
                                postbackHandler(userId, accountId, event.message.quick_reply.payload, context, callbackAfterParsingContext);
                            }

                        }

                        //fetch user info only if not already fetched
                        if( !context.getVal('userInfo') ) {
                            FacebookApiService.getUserInfo(userId, accessToken, function(error, userInfo) {
                                if(error) {
                                    console.log(error);
                                }
                                if( userInfo ) {
                                    context.setVal('userInfo', userInfo);
                                }
                                executeAfterFbUserInfoApiCall();
                            });
                        }
                        else {
                            executeAfterFbUserInfoApiCall();
                        }

                        return eventCallback();
                    });
                });
            });
        },
        function() {
            return messageCallback()
        });
    }, function() {

        //logs all the fb messages
        Logger.log(entries);
    });
};

var textMessageHandler = function(userId, accountId, text, context, callbackAfterParsingContext) {
    CommonHandler.textMessageHandler(userId, accountId, text, context, callbackAfterParsingContext);
};

var postbackHandler = function(userId, accountId, payload, context, callbackAfterParsingContext) {
    CommonHandler.postbackHandler(userId, accountId, payload, context, callbackAfterParsingContext);
};

//in this method we check if the message is sent by the page admin or not
var echoHandler = function(event) {

    //if no app_id is set means that the page admin is talking
    if( !event.message.app_id ) {

        //in case of echo messages sender id is the page and recepient id is the user
        var profileNetworkId  = event.sender.id;
        var userId = event.recipient.id;
        var profileNetworkName = 'facebook';

        ContextDao.getContextByUserIdNetworkInfo(userId, profileNetworkId, profileNetworkName, function(error, contextDoc) {
            if( error ) {
                return console.log(error);
            }
            if( !contextDoc ) {
                return console.log('no context found')
            }

            //stop the bot for 1 hour
            var currentDate = new Date();
            var timeToAdd = 1 * 60 * 60 * 1000;
            var restartTimestamp = currentDate.getTime() + timeToAdd;

            var query = {
                _id : contextDoc._id
            };

            var updateDoc = {
                $set : {
                    restartTimestamp : restartTimestamp
                }
            };

            ContextDao.updateContext(query, updateDoc, {}, function(error, result) {});

        });
    }
};
