var async = require('async');
var FbLoggerDao = require('../../daos/logger/fb');

exports.log = function( entries ) {

    async.each(entries, function(entry, entryCallback) {
        var messaging_list = entry.messaging;
        async.each(messaging_list, function(messaging, messageCallback) {

            //first call the callback
            messageCallback();

            try {

                //don't log delivery statuses
                if (messaging.delivery) {
                    return;
                }

                var userId;
                var pageId;

                //in case of echo senderId is pageId
                if (messaging.message && messaging.message.is_echo) {
                    pageId = messaging.sender.id;
                    userId = messaging.recipient.id;
                }
                else {
                    pageId = messaging.recipient.id;
                    userId = messaging.sender.id;
                }

                //doc to be logged
                var doc = {
                    pageId: pageId,
                    userId: userId,
                    message: messaging.message,
                    timestamp: messaging.timestamp
                };

                FbLoggerDao.create(doc, function (error, result) {});
            }
            catch(e) {
                //ignore exceptions
            }

        }, function() {
            return entryCallback();
        });
    });

};