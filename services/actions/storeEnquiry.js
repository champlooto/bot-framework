var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.storeEnquiryAction = function(accountId, context, callback) {
    // Offer him the link to website for store Enquiry

    var previousActionList = context.getVal('previousActionList');
    var previousAction = previousActionList[previousActionList.length -1];

    var factObj = {
        previousAction : previousAction
    };
    return callback(null, context, factObj);
};
