var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.ordinalAction = function(accountId, context, callback) {
    // Deciding Product Liked
    var numberObj = context.getVal('ordiconvert');
    var number = numberObj[0].value
    var previousActionList = context.getVal('previousActionList');
    var previousAction = previousActionList[previousActionList.length -1];

    var factObj = {
        previousAction : previousAction,
        number : number,
    };
    return callback(null, context, factObj);
};
