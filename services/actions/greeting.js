var _ = require('underscore');
var constants = require('../../constants');
var CustomizationDao = require('../../daos/customization');
var ProductDao = require('../../daos/product');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.greetingAction = function(accountId, context, callback) {

    CustomizationDao.getCustomizationByAccountId(accountId, function(error, customizationDoc) {
        if( error ) {
            console.log(error);
        }

        ProductDao.getLatestProduct(accountId, 0, 1, function(error, productList) {
            if( error ) {
                console.log(error);
            }

            var bannerImage = productList && _.isArray(productList) && productList.length > 0 && productList[0].imageUrl ? productList[0].imageUrl : 'https://www.gomage.com/wp/wp-content/uploads/2015/07/SaaS-Shopping-Carts.png';

            //check if the user is a first time user
            var firstTime = context.getVal('firstTime');

            // getting the brandName from customizationDoc
            var brandName;

            var gender = (context.getVal('userInfo') && context.getVal('userInfo').gender) ? context.getVal('userInfo').gender:'unknown'

            if( customizationDoc && customizationDoc.website && customizationDoc.website.length > 0 ) {
                brandName = customizationDoc.website;
            }

            var storeName = context.getVal('storeName')? context.getVal('storeName') : 'our store';
            storeName = brandName ? brandName : storeName;

            // setting the greetingCount
            var greetingCount = context.getVal('greetingCount');
            greetingCount = greetingCount ? greetingCount + 1 : 1;
            context.setVal('greetingCount', greetingCount);

            //Getting user's Info
            var firstName = context.getVal('userInfo') && context.getVal('userInfo').first_name ? context.getVal('userInfo').first_name : 'there';

            //setting messageLength
            var messageLength = context.getVal('text') && typeof context.getVal('text') === 'string' ? context.getVal('text').length : 0;

            //checking for noAI accounts
            var noAI;
            if( customizationDoc && customizationDoc.noAI ){
                noAI = true;
                context.setVal('noAI', true);
            }

            var factObj = {
                firstName : firstName,
                storeName : storeName,
                noAI : noAI,
                customizationDoc : customizationDoc,
                greetingCount : greetingCount,
                messageLength : messageLength,
                gender : gender,
                firstTime : firstTime,
                bannerImage : bannerImage
            };

            return callback(null, context, factObj);

        });
    });

};
