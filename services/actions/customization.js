var StoreDao = require('../../daos/store');
var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var _ = require('underscore');
var ProductDao = require('../../daos/product');

exports.customStoreAction = function(accountId, context, callback){
    StoreDao.getStoreList(accountId, function(error, storeList) {
        if(error) {
            return callback(error);
        }

        var factObj = {
          storeList : storeList
        }

        return callback(null, context, factObj);
    });
};

exports.emptyAction = function(accountId, context, callback){
    var factObj = {};
    return callback(null, context, factObj);
};

exports.customFindAction = function(accountId, context, callback){
    var skip = 0;
    var limit = 2;
    var storeId = context.getVal('store')[0].value;
    var firstName = context.getVal('userInfo') && context.getVal('userInfo').first_name ? context.getVal('userInfo').first_name : 'there';
    var callbackHandler = function(error, productList) {
        if(error) {
            return callback(error);
        }
        //Since Every Store Has Just One Entry
        var noImageTest = (productList[0].imageUrl!= "http://www.demo.com");

        var factObj = {
            firstName : firstName,
            productList : productList,
            noImageTest : noImageTest,
        };
        return callback(null, context, factObj);
    };

    var query = {
        storeId : storeId,
        accountId : accountId
    };

    ProductDao.getProduct(query, skip, limit, callbackHandler);
}
