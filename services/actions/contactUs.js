var CustomizationDao = require('../../daos/customization');
var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.contactUsAction = function(accountId, context, callback){

    var callbackHandler = function(error, customizationDoc){
        if( error ){
            console.log(error);
        }
         var contactInfo;
        if( customizationDoc && customizationDoc.contactInfo && customizationDoc.contactInfo.length > 0 ) {
            contactInfo = customizationDoc.contactInfo;
        }
        var factObj = {
          contactInfo : contactInfo
        }

        return callback(null, context, factObj);
    }
    CustomizationDao.getCustomizationByAccountId(accountId, callbackHandler);
}
