var _ = require('underscore');
var constants = require('../../constants');
var StoreDao = require('../../daos/store');
var CustomizationDao = require('../../daos/customization')
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.purchaseOptionAction = function(accountId, context, callback) {

    // setting purchaseOptionCount
    var purchaseOptionCount = context.getVal('purchaseOptionCount') ? context.getVal('purchaseOptionCount') + 1 : 1;
    context.setVal('purchaseOptionCount', purchaseOptionCount);

    var price = context.getVal('price');
    var color = context.getVal('color');

    //Getting Custom Search Query Example
    CustomizationDao.getCustomizationByAccountId(accountId, function(error, customizationDoc){
        if( error ) {
            console.log(error);
        }

        var customMessage = (customizationDoc && customizationDoc.customizedExample) ? customizationDoc.customizedExample : 'Text me something you want to find like "Dress under 10000rs" or "👗 under 10000rs" .';
        var previousActionList = context.getVal('previousActionList');
        var previousAction = previousActionList[previousActionList.length -1];

        StoreDao.getStoreList(accountId, function(error, storeList) {
            if( error ) {
                return callback(error);
            }

            var factObj = {
                storeList : storeList,
                customMessage : customMessage,
                purchaseOptionCount : purchaseOptionCount,
                previousAction : previousAction,
                price : price,
                color : color
            };

            return callback(null, context, factObj);
        });
    });
};
