var _ = require('underscore');
var constants = require('../../constants');
var ProductDao = require('../../daos/product');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var PRODUCT_COUNT_LIMIT = constants.PRODUCT_COUNT_LIMIT;

//returns all the entities that will be used while querying for the product
var getAllProductRelatedParamsFromContext = function(context) {
    var params = {};

    //if color has been set
    if( context.getVal('color') ) {
        var colorList = _.map(context.getVal('color'), function(colorObj) {
            return colorObj.value.trim().toLocaleLowerCase();
        });
        params.colorList = colorList;
    }
    else {
        params.colorList = [];
    }

    //if price has been set
    if( context.getVal('price') ) {
        var maxMoney = _.max(context.getVal('price'), function(moneyObj) {
            return moneyObj.value;
        });
        params.price = maxMoney.value;
    }

    //set gender if specified
    if( context.getFirstArrVal('gender') ) {
        params.gender = context.getFirstArrVal('gender');
    }

    //otherParams will be used as a hard query field
    params.otherParams = {};
    if( context.getVal('store') ) {
        params.otherParams.storeId = context.getFirstArrVal('store');
    }

    if( context.getVal('category') ) {
        params.otherParams.categoryId = {
            $in : context.getVal('category')
        };
    }

    return params;
};

//function return products
exports.findProductAction = function(accountId, context, callback) {

    var params = getAllProductRelatedParamsFromContext(context);
    var otherParams = params.otherParams;

    var keywordList  = context.getArrVal('keyword');
    var productSearchText = keywordList.join(' ');

    var productPageCount = context.getVal('productPageCount') || 0;
    var skip = productPageCount * PRODUCT_COUNT_LIMIT;

    ProductDao.getProductByAggregateQuery(
        accountId, productSearchText,
        params.gender, params.colorList, otherParams,
        skip, PRODUCT_COUNT_LIMIT + 1,
        function(error, productList) {
            if( error ) {
                return callback(error);
            }

            var factObj = {};
            factObj.skip = skip;

            //add all params to factObj
            factObj = _.extend(factObj, params);
            factObj.productList = productList;
            return callback(null, context, factObj);
        }
    );

};

exports.findSimilarProductAction = function(accountId, context, callback) {
    var factObj = {};
    var likedProductIdList = context.getVal('likedProductId');
    var latestLikedProductId = likedProductIdList.slice(-1)[0].value;
    ProductDao.getProductById(latestLikedProductId, accountId, function(error, productInfo) {
        if( error ) {
            return callback(error);
        }

        factObj.productInfo = productInfo;
        return callback(null, context, factObj);
    });
};

//ask user question about price or color, etc
exports.askProductQuestionAction = function(accountId, context, callback) {
    var factObj = {};
    var questionTags = ['color', 'price'];

    //check if already a tag is set or not
    var unAskedTags = _.filter(questionTags, function( qTag ) {
        return !context.getVal( qTag );
    });

    var likedProductIdList = context.getVal('likedProductId');
    if( likedProductIdList && likedProductIdList.length > 0 ) {
        var lastLikedProductIdDoc = likedProductIdList.slice(-1)[0];
        ProductDao.getProductById(lastLikedProductIdDoc.value, accountId, function(error, product) {
            if( error ) {
                return callback(error);
            }

            product = product ? product : {};
            factObj.product = product;

            //check if the unaskedTag is relevant to the product
            var unAskedTag;
            for( var i = 0; i < unAskedTags.length; i++ ) {
                if ( product[unAskedTags[i]] ) {
                    unAskedTag = unAskedTags[i];
                    break;
                }
            }

            factObj.unAskedTag = unAskedTag;
            return callback(null, context, factObj);
        });
    }
    else {
        return callback(null, context, factObj);
    }
};

exports.productInfoAction = function(accountId, context, callback) {
    var factObj = {};
    var productId = context.getFirstArrVal('infoProductId');
    ProductDao.getProductById(productId, accountId, function(error, productInfo) {
        if( error ) {
            return callback(error);
        }

        factObj.productInfo = productInfo;
        return callback(null, context, factObj);
    });
};

exports.showLikedProductsAction = function(accountId, context, callback) {
    var factObj = {};
    var likedProductIdList = context.getVal('likedProductId');

    if( likedProductIdList && _.isArray(likedProductIdList) ) {
        var productIdList = _.map(likedProductIdList, function (productObj) {
            return productObj.value;
        });

        var query = {
            accountId: accountId,
            _id: {
                $in: productIdList
            }
        };

        ProductDao.getProduct(query, 0, 2 * ( constants.PRODUCT_COUNT_LIMIT ), null, function (error, productList) {
            if (error) {
                return callback(error);
            }

            factObj.likedProductList = productList;
            return callback(null, context, factObj);
        });
    }
    else {
        factObj.productList = [];
        return callback(null, context, factObj);
    }
};