var _ = require('underscore');
var async = require('async');
var PostActionRules = require('../rules/post-action/index');
var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var LIST_OF_FILES  = require('require-dir')();

//extract the list of all the actions
var LIST_OF_ACTIONS = {};
_.each(LIST_OF_FILES, function(fileActions, fileName, fileObj) {
    _.each(fileActions, function(action, actionName, actionObj) {
        LIST_OF_ACTIONS[actionName] = action;
        if( actionName === 'purchaseOptionAction' ) {
            LIST_OF_ACTIONS['storeAction'] = action;
        }
    });
});

exports.executeActionList = function(accountId, actionList, context, callback) {
    var finalMessageQueue = [];

    //store the current actions that are being performed in the context obj
    context.setVal('actionList', actionList);

    var actionCount = 0;
    async.whilst(
        function() { return actionCount < context.getVal('actionList').length },
        function( actionCompletionCallback ) {
            var action = context.getVal('actionList')[actionCount];

            LIST_OF_ACTIONS[action](accountId, context, function(error, context, factObj) {
                actionCount++;
                if( error ) {
                    return actionCompletionCallback();
                }

                //current action executed is also added to facts object
                factObj.action = action;

                //add context too on the factObj
                factObj.context = context;

                //execute using the PostActionRules engine
                PostActionRules.execute(factObj, function(error, result) {

                    if( result.messageQueue ) {
                        finalMessageQueue = finalMessageQueue.concat( result.messageQueue );
                    }

                    if( result.nextActionList ) {
                        var currentActionList = context.getVal('actionList');
                        currentActionList = currentActionList.concat(result.nextActionList);
                        context.setVal('actionList', currentActionList);

                        var previousActionList = context.getVal('previousActionList') || [];
                        context.setVal('previousActionList', previousActionList.concat(currentActionList));
                    }

                    //add factObj to the context too
                    //useful for finite state machine
                    //delete factObj.context;
                    //context.setVal('factObj', factObj);

                    return actionCompletionCallback();
                });

            });
        },
        function(error) {

            //need to keep track of this for noIdeaAction
            context.setVal('previousActionList', context.getVal('actionList'));
            context.deleteVal('actionList');

            //set the previous messageQueue need this to check if the user has typed a text instead of button
            context.setVal('previousMessageQueue', finalMessageQueue);

            return callback(error, context, finalMessageQueue);
        }
    );
};
