var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.thankYouAction = function(accountId, context, callback) {
    var previousActionList = context.getVal('previousActionList');
    var previousAction = previousActionList[previousActionList.length -1];

    var factObj = {
        previousAction : previousAction,
    }
    return callback(null, context, factObj);
};
