var CustomizationDao = require('../../daos/customization');
var constants = require('../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;

exports.aboutUsAction = function(accountId, context, callback) {

    CustomizationDao.getCustomizationByAccountId(accountId, function(error, customizationDoc) {
        if( error ) {
            console.log(error)
        }

        var storeName = context.getVal('storeName') ? context.getVal('storeName') : 'our store';
        var aboutUs;

        if( customizationDoc && customizationDoc.aboutUs ) {
            aboutUs = customizationDoc.aboutUs;
        }
        else {
            aboutUs = "Welcome to " + storeName + ". You can use our messenger to drop us a message or browse products";
        }

        var factObj = {
            aboutUs : aboutUs
        };

        return callback(null, context, factObj);
    });
};
