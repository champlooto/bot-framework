var _ = require('underscore');
var constants = require('../constants');

function Context( data ) {
    this.version = constants.CONTEXT_VERSION;

    //fill data into context
    if( data && data.version &&  data.version === this.version ) {
        _.each(data, function(index, key) {
            this.setVal(key, data[key]);
        }.bind(this));
    }
};

Context.prototype.getVal = function( key ) {
    return this[key];
};

Context.prototype.setVal = function( key, value ) {
    this.sideEffects(key);
    this[key] = value;
};

Context.prototype.deleteVal = function( key ) {
    this.sideEffects(key);
    delete this[key];
};


//side effects related to a key change
Context.prototype.sideEffects = function(key, type) {

    if(
        key === 'color'
        || key === 'price'
        || key === 'store'
        || key === 'gender'
        || key === 'category'
        || key === 'store'
    ) {
        this.deleteVal('productPageCount');
    }

    if( key === 'keyword' ) {
        var productKeys = ['productPageCount', 'store', 'category', 'likedProductId'];
        productKeys.forEach(function(key) {
            this.deleteVal(key);
        }.bind(this));
    }

};

//returns first value of [{value:1}, {value:4}]
Context.prototype.getFirstArrVal = function(key) {
    var arrVal = this.getArrVal(key);
    if( arrVal.length > 0 ) {
        return arrVal[0];
    }
    return null;
};

//converts data from this [{value:1}] to [1]
Context.prototype.getArrVal = function(key) {
    var rawValue = this[key];
    if( rawValue && _.isArray(rawValue) ) {
        var arr = _.map(rawValue, function(obj) {
            return obj.value;
        });
        return arr;
    }
    return [];
};

module.exports  = Context;
