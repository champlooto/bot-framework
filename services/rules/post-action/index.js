/*
*
* Couple of rules to follow while writing your rules.
*  - Always write your messageQueue inside the rule
*  - If you messageQueue is outside of the rule i.e called using a function then add a nextActionList to your rule
*  - Always use Utils.trim() while writing an action in the messageQueue
*
*/

var RuleEngine = require('node-rules');
var _ = require('underscore');
var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var LIST_OF_FILES  = require('require-dir')();

var parseAndReturnRules = function() {
    var rules = [];

    _.each(LIST_OF_FILES, function(value, key, obj) {
        rules = rules.concat(value());
    });

      //create unique ids for rules so that the are easy to search and also extract the nexActionList
      //using Utils.trim and this.nextActionList = [] regex
    rules = _.map(rules, function(rule, index) {
        rule.name = rule.name + '-' + index;

        var nextActionList = [];
        if( rule.nextActionList ) {
            nextActionList = rule.nextActionList;
        }

        //find out the next actionList from the consequence function string
        var consequenceStr = rule.consequence.toString();
        var nextActionListStr = 'this.nextActionList';

        if ( consequenceStr.indexOf(nextActionListStr) > -1 ) {
            var nextActionListIndex = consequenceStr.indexOf(nextActionListStr) + nextActionListStr.length;

            //find the index of opening and closing braces
            var openingBraceIndex;
            var closingBraceIndex;
            while (true) {
                var char = consequenceStr[nextActionListIndex++];
                if (char === '[') {
                    openingBraceIndex = nextActionListIndex;
                }
                if (char === ']') {
                    closingBraceIndex = nextActionListIndex - 1;
                    break;
                }
            }

            var actionRaw = consequenceStr.substring(openingBraceIndex, closingBraceIndex).replace(/"/g, '').replace(/'/g, '').replace(/\s+/g, '');
            nextActionList = actionRaw.split(',');
        }

        else {

            var workingConsequenceStr = consequenceStr;

            //find actionList using Utils.trim
            while( true ) {
                var utilsTrimIndex = workingConsequenceStr.indexOf('Utils.trim');

                if( utilsTrimIndex === -1 ) {
                    break;
                }

                var openingBraceIndex;
                var closingBraceIndex;
                while( true ) {
                    var char = workingConsequenceStr[utilsTrimIndex++];

                    if( char === '(' ) {
                        openingBraceIndex = utilsTrimIndex;
                    }

                    if( char === ')' ) {
                        closingBraceIndex = utilsTrimIndex -1;
                        var action = workingConsequenceStr.substring(openingBraceIndex, closingBraceIndex);
                        action = action.replace(/"/g, '').replace(/'/g, '').replace(/\s+/g, '');
                        nextActionList.push(action);
                        workingConsequenceStr = workingConsequenceStr.substr(utilsTrimIndex);
                        break;
                    }
                }
            }
        }

        rule.nextActionList = _.uniq(nextActionList);
        nextActionList.forEach(function(action) {
            rule[action] =  true;
        });

        return rule;
    });
    return rules;
};

var rules = parseAndReturnRules();
//console.log(rules);

exports.execute = function(factObj, callback) {
    var R = new RuleEngine(rules);

    R.execute(factObj, function(result) {
        return callback(null, result);
    });
};

//var R = new RuleEngine(rules);
//var rules = R.findRules({ askProductQuestionAction : true });
//console.log(rules);

//var factObj = {
//    productInfo: {
//        productName: 'Fashionable Blue 3/4th Jeans for Women',
//        description: ' ',
//        tagList: [ '3/4th', 'fashionable', 'jeans', 'women' ],
//        productImpression: 54,
//        productClick: 28,
//        storeId: [ '57c2be7759d0c45c077d2745' ],
//        categoryId: [ '57c2be8793e0150650dd8b89',
//                '57c2be8793e0150650dd8b8a',
//                '57c2be7c93e0150650dd8b7c',
//                '57c2be8693e0150650dd8b85' ],
//        color: 'blue',
//        integrationType: 'shopify',
//        webUrl: 'https://fashion-store-61.myshopify.com/products/fashionable-blue-3-4th-jeans-for-women',
//        price: 2300,
//        accountId: '57c2be1f59d0c45c077d2742',
//        createdAt: 1472280926000,
//        shopifyRawData: {
//            image: [],
//            images: [{ variant_ids: [],
//                src: 'https://cdn.shopify.com/s/files/1/1453/7122/products/jeansw4.jpg?v=1472280927',
//                updated_at: '2016-08-27T02:55:27-04:00',
//                created_at: '2016-08-27T02:55:27-04:00',
//                position: 1,
//                product_id: 8262795334,
//                id: 17542472902 }],
//            options: [],
//            variants: [],
//            tags: '3/4th, blue, fashionable, jeans, women',
//            published_scope: 'global',
//            template_suffix: null,
//            published_at: '2016-08-27T02:53:00-04:00',
//            updated_at: '2016-08-27T02:55:27-04:00',
//            handle: 'fashionable-blue-3-4th-jeans-for-women',
//            created_at: '2016-08-27T02:55:26-04:00',
//            product_type: '',
//            vendor: 'Fashion Store',
//            body_html: '',
//            title: 'Fashionable Blue 3/4th Jeans for Women',
//            id: 8262795334
//        },
//        mediaId: 8262795334,
//        imageUrl: 'https://cdn.shopify.com/s/files/1/1453/7122/products/jeansw4.jpg?v=1472280927',
//        _id: '57c2be7959d0c45c077d2752'
//    },
//    action: 'productInfoAction'
//};
//
//exports.execute(factObj, function(error, result) {
//    console.log(result);
//});
