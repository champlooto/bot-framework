var _ = require('underscore');
var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');
var PRODUCT_COUNT_LIMIT = constants.PRODUCT_COUNT_LIMIT;

var getRules = function() {
    return [
        {
            name : 'findProductAction',
            condition : function(R) {
                R.when(
                    (
                        this.action === 'findProductAction'
                    )
                    && this.productList.length > 0
                );
            },
            consequence : function(R) {
                var context = this.context;
                var newProductPageCount =  context.getVal('productPageCount') ? context.getVal('productPageCount') + 1 : 1;
                context.setVal('productPageCount', newProductPageCount);

                var productListCount = this.productList.length;
                var productList = this.productList.slice(0, 5);

                var getProductMessage = function(productList) {
                    var productOptions = _.map(productList, function( product ) {
                        var obj = {
                            imageUrl : product.imageUrl,
                            name : product.productName,
                            options : [
                                {
                                    title : 'Learn More',
                                    payload : 'infoProductId,' + product._id + ',' + Utils.trimAction('productInfoAction')
                                },
                                {
                                    title : 'Share'
                                },
                                {
                                    title : 'Buy on Website',
                                    url : product.webUrl
                                }
                            ]
                        };

                        var price = product.price ? product.price : null;
                        if( price ) {
                            obj.subtitle = 'Price : ' + price;
                        }
                        return obj;
                    });

                    //if we can show more products to the user then show the show more button options
                    if( productListCount > PRODUCT_COUNT_LIMIT ) {
                        productOptions.push({
                            imageUrl : 'http://vignette2.wikia.nocookie.net/looneytunes/images/e/e1/All.jpg/revision/latest?cb=20150313020828',
                            name : 'Click "More" to view more options.',
                            options : [
                                {
                                    title : 'More',
                                    payload : 'more,true,' + Utils.trimAction('askProductQuestionAction')
                                }
                            ]
                        });
                    }

                    var productMessageQueue = [
                        {
                            type : MESSAGE_TYPE.PRODUCT,
                            data : productOptions
                        },
                        {
                            type : MESSAGE_TYPE.TEXT,
                            data : 'You can also search for something specific by typing \n \n ➡ @bot find me a blue shirt under $2000 '
                        }
                    ];

                    return productMessageQueue;
                };
                var messageQueue = getProductMessage(productList);

                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 1
        },
        {
            name : 'findProductAction',
            condition : function(R) {
                R.when(
                    (
                        this.action === 'findProductAction'
                    )
                    && this.productList.length === 0
                    && this.skip > 0
                );
            },
            consequence : function(R) {
                var messageQueue = [{
                    type : MESSAGE_TYPE.TEXT,
                    data : 'I can\'t find any more of these.'
                }];
                this.messageQueue = messageQueue;
                this.nextActionList = ['showLikedProductsAction'];
                R.stop();
            },
            priority : 1
        },
        {
            name : 'findProductAction',
            condition : function(R) {
                R.when(
                    this.action === 'findProductAction'
                    && this.productList.length === 0
                    && this.skip === 0
                );
            },
            consequence : function(R) {
                var messageQueue = [{
                    type : MESSAGE_TYPE.TEXT,
                    data : 'Sorry I couldn\'t find what you were looking for.'
                }];
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 1
        },
        {
            name : 'askProductQuestionAction',
            condition : function(R) {
                R.when(
                    this.action === 'askProductQuestionAction'
                    && (this.unAskedTag === 'price' || this.unAskedTag === 'color')
                )
            },
            consequence : function(R) {
                var product = this.product;
                var unAskedTag = this.unAskedTag;
                var unAskedTagValues = product[unAskedTag];
                var button = [];

                if( !_.isArray(unAskedTagValues) ) {
                    unAskedTagValues = [unAskedTagValues];
                }

                _.each(unAskedTagValues, function(unAskedTagValue) {
                    button.push({
                        title : unAskedTagValue + '',
                        payload : unAskedTag + ',' + unAskedTagValue + ',' + Utils.trimAction('findProductAction')
                    });
                });

                var question;
                if( unAskedTag === 'price' ) {
                    question = 'What is your price range? You can type \n  \n ➡ @bot under $5000';
                }
                else if( unAskedTag === 'color' ) {
                    question = 'Which color do you like the most? You can type \n \ ➡ @bot I like blue';
                }

                this.messageQueue = [{
                    type : MESSAGE_TYPE.QUICK_REPLY,
                    title : question,
                    data : button
                }];
                R.stop();
            }
        },
        {
            name : 'askProductQuestionAction',
            condition : function(R) {
                R.when(
                    this.action === 'askProductQuestionAction'
                    && !this.unAskedTag
                )
            },
            consequence : function(R) {
                this.nextActionList = ['findProductAction'];
                R.stop();
            }
        },
        {
            name : 'productInfoAction',
            condition : function(R) {
                R.when(
                    this.action === 'productInfoAction'
                    && this.productInfo
                );
            },
            consequence : function(R) {
                var product = this.productInfo;
                var productId = product._id;
                var description = product.description || '';
                var images =  product.shopifyRawData ?  product.shopifyRawData.images || []  : [];
                var messageQueue = [];

                if( description.trim().length > 0 ) {
                    messageQueue.push({
                        type: MESSAGE_TYPE.TEXT,
                        data: 'Some more info on the product \n' + description
                    });
                }

                if( images && images.length > 0 ) {
                    var productImagesTemplate = _.map(images, function(image, index) {
                        var obj = {
                            imageUrl : image.src,
                            name : 'Product Image - ' + (index + 1),
                            options : [
                                {
                                    title : 'Share'
                                },
                                {
                                    title : 'Buy on Website',
                                    url : product.webUrl
                                }
                            ]
                        };
                        var price = product.price ? product.price : null;
                        if( price ) {
                            obj.subtitle = 'Price : ' + price;
                        }
                        return obj;
                    });

                    messageQueue.push({
                        type : MESSAGE_TYPE.PRODUCT,
                        data : productImagesTemplate
                    });
                }

                messageQueue.push({
                    type : MESSAGE_TYPE.QUICK_REPLY,
                    title : 'Click on "Buy on Website" button to purchase this product. You can also select from the options below',
                    data : [
                        {
                            title : 'Show different',
                            payload : 'more,true,' + Utils.trimAction('askProductQuestionAction')
                        },
                        {
                            title : 'Show similar',
                            payload : 'likedProductId,' + productId + ',' + Utils.trimAction('findSimilarProductAction')
                        }
                    ]
                });
                this.messageQueue = messageQueue;

                R.stop();
            }
        },
        {
            name: 'showLikedProductsAction',
            condition: function (R) {
                R.when(
                    this.action === 'showLikedProductsAction'
                    && this.likedProductList
                    && this.likedProductList.length > 0
                );
            },
            consequence: function (R) {
                var likedProductList = this.likedProductList;
                var productTemplate = _.map(likedProductList, function (product) {
                    var obj = {
                        imageUrl: product.imageUrl,
                        name: product.productName,
                        options: [
                            {
                                title: 'Buy on Website',
                                url: product.webUrl
                            },
                            {
                                title: 'Share'
                            }
                        ]
                    };

                    var price = product.price ? product.price : null;
                    if (price) {
                        obj.subtitle = 'Price : ' + price;
                    }
                    return obj;
                });
                this.messageQueue = [
                    {
                        type: MESSAGE_TYPE.TEXT,
                        data: 'Would like to buy any one of the below products?'
                    },
                    {
                        type: MESSAGE_TYPE.PRODUCT,
                        data: productTemplate
                    }
                ];
                R.stop();
            },
            priority: 1,
        },
        {
            name : 'showLikedProductsAction',
            condition : function(R) {
                R.when(
                    this.action === 'showLikedProductsAction'
                    && this.likedProductList
                    && this.likedProductList.length === 0
                );
            },
            consequence : function(R) {
                this.messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'I have run out of products for you :('
                    }
                ];
                R.stop();
            },
            priority : 1
        },
        {
            name : 'findSimilarProductAction',
            condition : function(R) {
                R.when(
                    this.action === 'findSimilarProductAction'
                );
            },
            consequence : function(R) {
                var context = this.context;
                var productInfo = this.productInfo;

                if( productInfo &&  productInfo.categoryId ) {
                    var categoryIdList = productInfo.categoryId;

                    //if not an array then create a new array
                    if( !_.isArray( categoryIdList ) ) {
                        categoryIdList = [categoryIdList];
                    }

                    //merge with current categoryId
                    var currentCategoryList = context.getVal('category');
                    if( currentCategoryList && _.isArray( currentCategoryList ) ) {
                        categoryIdList = _.uniq(currentCategoryList.concat(categoryIdList));
                    }
                    context.setVal('category', categoryIdList);
                }

                this.nextActionList = ['askProductQuestionAction'];
                R.stop();
            }
        }
    ];
};


module.exports = getRules;
