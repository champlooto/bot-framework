var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'customStoreAction',
            condition : function(R) {
                R.when(
                    this.action === 'customStoreAction'
                )
            },
            consequence : function(R) {
                var options = _.map(this.storeList, function(store) {
                    return {
                        title : store.name,
                        payload : 'store,' + store._id + ','+ Utils.trimAction('customFindAction')
                    }
                });
                var optionMessage = {
                    title : 'Please choose from the option:',
                    type : MESSAGE_TYPE.OPTION,
                    data : options
                };
                var messageQueue = [optionMessage];
                this.messageQueue = messageQueue
                R.stop();
            },
            priority : 1
        },
        {
            name : 'customStoreAction',
            condition : function(R) {
                R.when(
                    this.action === 'customStoreAction' && (this.storeList.length === 0)
                )
            },
            consequence : function(R) {
                var messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'We are not selling anything yet!! Thanks for your interest and stay tuned for any updates'
                    }
                ];
                this.messageQueue = messageQueue
                R.stop();
            },
            priority : 2
        },
        {
            name : 'emptyAction',
            condition : function(R) {
                R.when(
                    this.action === 'emptyAction'
                )
            },
            consequence : function(R) {
                this.messageQueue = [];
                R.stop();
            },
            priority : 1
        },
        {
            name : 'customFindAction',
            condition : function(R) {
                R.when(
                    this.action === 'customFindAction'
                )
            },
            consequence : function(R) {
              var messageQueue = [
                      {
                          type : MESSAGE_TYPE.TEXT,
                          data : 'Thanks for checking us out, ' + this.firstName+ '. '+ this.productList[0].description,
                      }
                  ];


                this.messageQueue = messageQueue
                R.stop();
            },
            priority : 1
        },
        {
            name : 'customFindAction',
            condition : function(R) {
                R.when(
                    this.action === 'customFindAction' && (this.noImageTest)
                )
            },
            consequence : function(R) {
              var data = {
                      imageUrl : this.productList[0].imageUrl,
                      name : this.productList[0].productName,
                      options : [
                          {
                              title : 'Go to Website',
                              url : this.productList[0].webUrl
                          }
                      ]
                  };

                  var messageQueue = [
                      {
                          type : MESSAGE_TYPE.PRODUCT,
                          data : [data]
                      }
                  ];
                this.messageQueue = messageQueue
                R.stop();
            },
            priority : 2
        },
    ];
};


module.exports = getRules;
