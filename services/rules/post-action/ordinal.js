var constants = require('../../../constants');
var _ = require('underscore');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'ordinalAction',
            condition : function(R) {
                R.when(
                    this.action === 'ordinalAction' && this.previousAction === 'askTagFilterAction'
                )
            },
            consequence : function(R) {
                var messageQueue = this.context.getVal('previousMessageQueue');
                var lastMessage = messageQueue[messageQueue.length-1];
                if(lastMessage.type === MESSAGE_TYPE.QUICK_REPLY){
                  var payload = lastMessage.data[number-1].payload;
                  var splitArr = payload.split(',');
                  var categoryId = splitArr[1];
                  if ( typeof  categoryId === 'string' ) {

                     selectedCategoryIdArray.push(categoryId);
                     selectedCategoryIdArray = _.uniq(selectedCategoryIdArray);
                     var newSelectedCategoryIdDocList = _.map(selectedCategoryIdArray, function (_id) {
                         return {wit: false, value: _id}
                     });

                     //refresh category list
                     this.context.setVal('category', newSelectedCategoryIdDocList);
                     this.context.deleteVal('productPageCount');
                  this.nextActionList = ['findProductAction'];
                  R.stop();
            }
          }
        },
            priority : 2
        },
        {
            name : 'ordinalAction',
            condition : function(R) {
                R.when(
                  this.action === 'ordinalAction'  && (this.previousAction === 'findProductAction')
                )
            },
            consequence : function(R) {
              var messageQueue = context.getVal('previousMessageQueue');
              this.action === 'ordinalAction'
              var lastMessage = messageQueue[0];
              if(lastMessage.type === MESSAGE_TYPE.PRODUCT){
                  var splitArr = lastMessage.data[number-1].options[1].payload.split(',')
                  var likedProductId = splitArr[1];
                  var likedProductObj = [{
                    value : likedProductId
                  }]
                  this.context.setVal('likedProductId', likedProductObj);
                }
                this.nextActionList = ['askTagFilterAction'];
                R.stop();
            },
            priority : 2
        },
        {
          name : 'ordinalAction',
          condition : function(R){
            R.when(
              this.action === 'ordinalAction'
            )
          },
          consequence : function(R){
            var messageQueue = [
              {
                type : MESSAGE_TYPE.TEXT,
                data : ':(',
              }
            ]
            this.messageQueue = messageQueue;
            R.stop();
          },
          priority : 1
        },
    ];
};


module.exports = getRules;
