var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'storeEnquiryAction',
            condition : function(R) {
                R.when(
                    this.action === 'storeEnquiryAction'
                )
            },
            consequence : function(R) {
              var messageQueue = [
                  {
                      type : MESSAGE_TYPE.TEXT,
                      data : 'I am a shopping bot that can only help you in finding products. :-('
                  },
              ];
              this.messageQueue = messageQueue;
              this.nextActionList = ['leaveMessageAction'];
              R.stop();
            },
            priority : 1
        },
    ];
};


module.exports = getRules;
