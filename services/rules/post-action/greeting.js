var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var generateGreetingMessage = function(greetingMessage, displayProductImage, buttons, storeName) {
    var messageQueue = [
        {
            type : MESSAGE_TYPE.TEXT,
            data : greetingMessage
        },
        {
            type : MESSAGE_TYPE.TEXT,
            data : 'Please drop us a message or select from the options below:'
        },
        {
            type : MESSAGE_TYPE.PRODUCT,
            data : [
                {
                    name : storeName,
                    imageUrl : displayProductImage,
                    options : buttons
                }
            ]
        },
        {
            type : MESSAGE_TYPE.TEXT,
            data : 'You also talk to our shopping bot by typing "@bot" .\n \n ➡ @bot What do you sell?'
        }
    ];
    return messageQueue;
};

var getRules = function() {
    return [
        {
            name : 'greetingAction',
            condition : function(R) {
                R.when(
                    this.action === 'greetingAction'
                    && this.firstTime === true
                );
            },
            consequence : function(R) {

                //for first time user
                var greetingMessage = 'Hi ' + this.firstName + ', Thank you for reaching ' + this.storeName + '. :-)';

                var buttons = [
                    {
                        title : '👜 Shop Now',
                        payload : 'restart,true,' + Utils.trimAction('purchaseOptionAction')
                    },
                    {
                        title : '🌟 Fresh Arrivals',
                        payload : 'restart,true,' + Utils.trimAction('latestProductAction'),
                    }
                ];

                var messageQueue = generateGreetingMessage(greetingMessage, this.bannerImage, buttons, this.storeName);
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 2
        },

        {
            name : 'greetingAction',
            condition : function(R) {
                R.when(
                    this.action === 'greetingAction'
                );
            },
            consequence : function(R) {

                //for returning user
                var greetingMessage = 'Hi ' + this.firstName + ', Welcome back :)';

                var buttons = [
                    {
                        title : '👜 Shop Now',
                        payload : 'restart,true,' + Utils.trimAction('purchaseOptionAction')
                    },
                    {
                        title : '🌟 Fresh Arrivals',
                        payload : 'restart,true,' + Utils.trimAction('latestProductAction'),
                    }
                ];

                var messageQueue = generateGreetingMessage(greetingMessage, this.bannerImage, buttons, this.storeName);
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 1
        },

        //No AI response needs to worked on and finalised
        {
            name : 'greetingAction',
            condition : function(R){
                R.when(
                    this.action === 'greetingAction'
                    && this.noAI
                );
            },
            consequence : function(R) {
                var greetingOptions = this.customizationDoc.greetingOptions;
                var data = [
                    {
                        title : greetingOptions[0],
                        payload : 'intent,shop,customStore',
                    }
                ];

                this.messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'Hey ' + this.firstName + ', Welcome to ' + this.storeName + '\n',
                    },
                    {
                        type : MESSAGE_TYPE.OPTION,
                        title : ":-)",
                        data : data,
                    }
                ];
                R.stop();
            },
            priority : 3
        }
    ];
};

module.exports = getRules;
