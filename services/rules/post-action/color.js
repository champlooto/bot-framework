var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'colorAction',
            condition : function(R) {
                R.when(
                    this.action === 'colorAction'
                )
            },
            consequence : function(R) {
                this.nextActionList = ['purchaseOptionAction'];
                R.stop();
            },
            priority : 1
        },
        {
            name : 'colorAction',
            condition : function(R) {
                R.when(
                    this.action === 'colorAction'
                    &&
                    (
                        this.previousAction === 'findProductAction'
                        || this.previousAction === 'askProductQuestionAction'
                        || this.previousAction === 'productInfoAction'
                        || this.previousAction === 'findSimilarProductAction'
                    )
                )
            },
            consequence : function(R) {
                this.nextActionList = ['findProductAction'];
                R.stop();
            },
            priority : 2
        }
    ];
};


module.exports = getRules;
