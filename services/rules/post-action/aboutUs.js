var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {
    return [
        {
            name : 'aboutUsAction',
            condition : function(R) {
                R.when( this.action === 'aboutUsAction' );
            },
            consequence : function(R) {
                var aboutUs = this.aboutUs;
                var messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : aboutUs,
                    },
                    {
                        type : MESSAGE_TYPE.QUICK_REPLY,
                        title : 'Type SHOP and start looking for products using our shopping assistant bot',
                        data : [
                          {
                              title : '👜 Shop Now',
                              payload : 'intent,shop,' + Utils.trimAction('purchaseOptionAction')
                          },
                          {
                              title : '👦 Need Help',
                              payload : 'intent,complaint,' + Utils.trimAction('leaveMessageAction'),
                          },
                        ]
                    }
                ];
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 1
        }
    ];
};


module.exports = getRules;
