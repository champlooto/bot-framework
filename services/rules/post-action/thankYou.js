var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'thankYouAction',
            condition : function(R) {
                R.when(
                    this.action === 'thankYouAction'
                )
            },
            consequence : function(R) {
                var messageQueue =[
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : '^_^'
                    },
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'My pleasure. Let me know if I can help you with anything else.'
                    },
                    {
                        type : MESSAGE_TYPE.QUICK_REPLY,
                        title : 'What would you like to do?',
                        data : [
                            {
                                title : 'Check Out Store',
                                payload : 'intent,shop,' + Utils.trimAction('purchaseOptionAction')
                            }
                        ]
                    }
                ];

                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 1
        },
        {
            name : 'thankYouAction',
            condition : function(R) {
                R.when(
                    this.action === 'thankYouAction'
                    && (this.previousAction === 'findProductAction')
                )
            },
            consequence : function(R) {
                var messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'Mention not. :)'
                    },
                    {
                        type : MESSAGE_TYPE.QUICK_REPLY,
                        title : 'What would you like to do?',
                        data : [
                            {
                              title : 'Continue Shopping',
                              payload : 'intent,shop,' + Utils.trimAction('findProductAction')
                            }
                        ]
                    }
                ];
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 2
        },
    ];
};


module.exports = getRules;
