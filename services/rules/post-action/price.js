var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'priceAction',
            condition : function(R) {
                R.when(
                    this.action === 'priceAction'
                )
            },
            consequence : function(R) {
                this.nextActionList = ['purchaseOptionAction'];
                R.stop();
            },
            priority : 1
        },
        {
            name : 'priceAction',
            condition : function(R) {
                R.when(
                    this.action === 'priceAction'
                    &&
                    (
                        this.previousAction === 'findProductAction'
                        || this.previousAction === 'askProductQuestionAction'
                        || this.previousAction === 'productInfoAction'
                        || this.previousAction === 'findSimilarProductAction'
                    )
                )
            },
            consequence : function(R) {
                this.nextActionList = ['findProductAction'];
                R.stop();
            },
            priority : 2
        },
    ];
};


module.exports = getRules;
