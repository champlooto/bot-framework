var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');
var _ = require('underscore');

var getRules = function() {

    return [
        {
            name : 'purchaseOptionAction',
            condition : function(R) {
                R.when(
                    this.action === 'purchaseOptionAction'
                    && this.storeList.length > 1
                );
            },
            consequence : function(R) {
                var storeList = this.storeList;
                var options = _.map(storeList, function(store) {
                    var obj = {
                        title : store.name,
                        payload : 'store' + ',' + store._id + ',' + Utils.trimAction('findProductAction')
                    };
                    return obj;
                });

                var messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : '(y)'
                    },
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'Our shopping bot can help you 🔎 for products. You can include your color or budget preferences. :-)   \n \n ➡ "@bot find me a Black Formal Shirt under 2000rs"'
                    },
                    {
                        type : MESSAGE_TYPE.QUICK_REPLY,
                        title : 'You can also choose from the options below:',
                        data : options
                    }
                ];
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 1
        },
        {
            name : 'purchaseOptionAction',
            condition : function(R) {
                R.when(
                    this.action === 'purchaseOptionAction'
                    && this.storeList.length > 1
                    && (this.previousAction === 'colorAction' || this.previousAction === 'priceAction')
                );
            },
            consequence : function(R) {
                var price = this.price;
                var color = this.color
                var confirmMessage = 'Looking for something ?';

                if ( color && price ) {
                    confirmMessage = 'Looking for a product in '+ color +' under '+ price + '? 🤔';
                }
                else if( price ) {
                    confirmMessage = 'Looking for something under ' + price + '? 🤔';
                }
                else if( color ) {
                    confirmMessage = 'Looking for something in ' + color + '? 🤔';
                }

                var storeList = this.storeList;
                var options = _.map(storeList, function(store) {
                    var obj = {
                        title : store.name,
                        payload : 'store' + store._id + ',' + Utils.trimAction('findProductAction')
                    };
                    return obj;
                });

                var messageQueue = [
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : confirmMessage
                    },
                    {
                        type : MESSAGE_TYPE.TEXT,
                        data : 'Type the name of the product you are searching. \n \n ➡ @bot shirt \n ➡@bot 👕'
                    },
                    {
                        type : MESSAGE_TYPE.QUICK_REPLY,
                        title : 'You can also click on the options below:',
                        data : options
                    }
                ];
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 2
        },
        {
            name : 'purchaseOptionAction',
            condition : function(R) {
                R.when(
                    this.action === 'purchaseOptionAction'
                    && this.storeList.length <= 1
                );
            },
            consequence : function(R) {
                this.nextActionList = ['findProductAction'];
                R.stop();
            },
            priority : 1
        },
    ];
};


module.exports = getRules;
