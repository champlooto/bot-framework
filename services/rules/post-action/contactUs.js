var constants = require('../../../constants');
var MESSAGE_TYPE = constants.MESSAGE_TYPE;
var Utils = require('../../utils');

var getRules = function() {

    return [
        {
            name : 'contactUsAction',
            condition : function(R) {
                R.when(
                    this.action === 'contactUsAction' && this.contactInfo
                )
            },
            consequence : function(R) {
                var messageQueue = [
                    {
                        type : MESSAGE_TYPE.QUICK_REPLY,
                        title : 'You can reach us at ' + this.contactInfo + ' or you can leave us a message.',
                        data : [
                            {
                                title : '👜 Shop Now',
                                payload : 'intent,shop,' + Utils.trimAction('purchaseOptionAction')
                            }
                        ]
                    }
                ];
                this.messageQueue = messageQueue;
                R.stop();
            },
            priority : 2
        },

    ];
};


module.exports = getRules;
