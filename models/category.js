var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CategorySchema = new Schema({
    name : {
        type : String,
        required : true
    },
    storeId : {
        type : String,
        required : true
    },
    accountId : {
        type : String,
        required : true
    },
    categoryImpression : {
        type : Number,
        default : 0
    },
    isActive : {
        type : Boolean,
        default : null,
    }
});

module.exports = mongoose.model('Category', CategorySchema);
