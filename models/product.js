var validate = require('mongoose-validator');
var mongoose = require('mongoose');
var validUrl = require('valid-url');

var Schema = mongoose.Schema;


//Adding productClick amd productImpression for Analytics
var ProductSchema = new Schema({
    imageUrl : {
        type : String,
        required : true,
        validate : {
            validator : function(value) {
                return validUrl.isUri(value);
            },
            message : 'Url is invalid'
        }
    },
    webUrl : {
        type : String,
        required : true,
        validate : {
            validator : function(value) {
                return validUrl.isUri(value);
            },
            message : 'Url is invalid'
        }
    },
    productName : {
        type: String,
        default : '',
    },
    description : {
        type : String,
        default : ''
    },
    categoryId : {
        type : Schema.Types.Mixed,
        required : false
    },
    categoryName : {
        type : Schema.Types.Mixed,
        required : false
    },
    storeId : {
        type : Schema.Types.Mixed,
        required : false
    },
    storeName : {
        type : Schema.Types.Mixed,
        required : true
    },
    createdAt : {
        type : Number,
        required : true
    },
    accountId : {
        type : String,
        required : true
    },
    tagList : {
        type : Array,
        required : false
    },
    productImpression : {
        type : Number,
        default : 0
    },
    productClick : {
        type : Number,
        default : 0
    },
    price : {
        type : Number,
        required : false
    },
    color : {
        type : Schema.Types.Mixed,
        required : false
    },
    productType : {
        type : String,
        required : false
    },
    shopifyRawData : {
        type : Schema.Types.Mixed,
        required : false
    }
});

module.exports = mongoose.model('Product', ProductSchema);
