var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var UserSchema = new Schema({
    userId : {
        type : String,
        required : true
    },
    accountId : {
        type : String,
        required : true
    },
    userInfo : {
        type : Schema.Types.Mixed,
        required : true
    },
    gender : {
        type : String,
        required : false
    },
    likedProduct : {
        type: Array,
        required : false
    }
});

module.exports = mongoose.model('User', UserSchema);
