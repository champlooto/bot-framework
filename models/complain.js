var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ComplainSchema = new Schema({
    issue : {
        type : String,
        required : true
    },
    userId : {
        type : String,
        required : true
    },
    accountId : {
        type : String,
        required : true
    },
    isResolved :{
      type : Number,
      required : true
    },
    userInfo : {
        type : Schema.Types.Mixed,
        required : false
    },
    issueType : {
      type : String,
      required : false
    }
});

module.exports = mongoose.model('Complain', ComplainSchema);
