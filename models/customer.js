var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CustomerSchema = new Schema({
    customerCount : {
        type : Number,
        default : 0,
        required : true
    },
    interactionCount :{
      type: Number,
      default: 0,
      required : true
    },
    accountId : {
        type : String,
        required : true
    }
});

module.exports = mongoose.model('Customer', CustomerSchema);
