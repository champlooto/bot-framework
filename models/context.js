var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ContextSchema = new Schema({
    userId : {
        type : String,
        required : true
    },
    profileNetworkId : {
        type : String,
        required : true
    },
    profileNetworkName : {
        type : String,
        required : true
    },
    accountId : {
        type : String,
        required : true
    },
    context : {
        type : Schema.Types.Mixed,
        required : false
    },
    restartTimestamp : {
        type : Number,
        required : false
    }
});

module.exports = mongoose.model('Context', ContextSchema);
