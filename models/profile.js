var validate = require('mongoose-validator');
var mongoose = require('mongoose');
var validUrl = require('valid-url');

var Schema = mongoose.Schema;

var ProfileSchema = new Schema({
    profileUsername : {
        type : String,
        required : true
    },
    profileNetworkId : {
        type : String,
        required : false
    },
    profileNetworkName : {
        type : String,
        required : true
    },
    token : {
        type : String
    },
    accountId : {
        type : String,
        required : true
    },
    isActive : {
        type : Number,
        default : 1
    }
});

module.exports = mongoose.model('Profile', ProfileSchema);
