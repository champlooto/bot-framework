var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CustomizationSchema = new Schema({
    website : {
        type : String,
        required : false,
    },
    aboutUs : {
        type : String,
        required : false,
    },
    accountId : {
        type : String,
        required : true
    },
    contactInfo : {
        type : String,
        required : false,
    },
    customizedExample : {
        type : String,
        required : false,
    },
    greetingMessage: {
        type: String,
        required : false,
    },
    greetingOptions: {
        type : Array,
        required : false,
    },
    noAI : {
        type : Number,
        required : false,
    }
});

module.exports = mongoose.model('Customization', CustomizationSchema);
