var validate = require('mongoose-validator');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var validUrl = require('valid-url');

var SALT_WORK_FACTOR = 10;

var Schema = mongoose.Schema;

var emailValidator = validate({
    validator : 'isEmail',
    message : 'Please enter a valid email address'
});

var AccountSchema = new Schema({
    email : {
        type : String,
        required : true,
        index: { unique: true },
        validate : emailValidator
    },
    password : {
        type : String,
        required : true
    }
});

AccountSchema.pre('save', function(next) {
    var account = this;

    //convert email to lowercase
    account.email = account.email.toLowerCase();

    // only hash the password if it has been modified (or is new)
    if ( !account.isModified('password') ) {
        return next();
    }

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) {
            return next(err);
        }

        // hash the password using our new salt
        bcrypt.hash(account.password, salt, function(err, hash) {
            if (err) {
                return next(err);
            }

            // override the cleartext password with the hashed one
            account.password = hash;
            next();
        });
    });

});

AccountSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) {
            return cb(err);
        }
        return cb(null, isMatch);
    });
};

module.exports = mongoose.model('Account', AccountSchema);
