var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var StoreSchema = new Schema({
    name : {
        type : String,
        required : true
    },
    accountId : {
        type : String,
        required : true
    },
    storeImpression : {
      type : Number,
      default : 0,
    },
    isActive : {
      type : Boolean,
      default : null,
    },
    shopifyRawData: {
      type: Schema.Types.Mixed,
      default:null,
    }
});

module.exports = mongoose.model('Store', StoreSchema);
