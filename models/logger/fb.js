var validate = require('mongoose-validator');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var FbLoggerSchema = new Schema({
    userId : {
        type : String,
        required : true
    },
    pageId : {
        type : String,
        required : true
    },
    message : {
        type : Schema.Types.Mixed
    },
    timestamp : {
        type : Number
    }
});

module.exports = mongoose.model('fb-log', FbLoggerSchema);
